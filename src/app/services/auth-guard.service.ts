import { Injectable } from '@angular/core';
import { CanActivate,ActivatedRouteSnapshot,RouterStateSnapshot,Router } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

  private logged:boolean=false;

  constructor(private router:Router) { }

  changeLogged(d:boolean){
    this.logged=d;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    //console.log("ActivatedRouteSnapshot",route.toString());
    //console.log("RouterStateSnapshot",state.toString());
    if(this.logged) return true;
    this.router.navigate(['/login']);//if not logged in then
    return false;
  }

}
