/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthOuterService } from './auth-outer.service';

describe('AuthOuterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthOuterService]
    });
  });

  it('should ...', inject([AuthOuterService], (service: AuthOuterService) => {
    expect(service).toBeTruthy();
  }));
});
