import { Injectable } from '@angular/core';
//import { Observable } from 'rxjs/Observable';
//import { Http,Headers } from '@angular/http';
//import { environment } from '../../environments/environment';
import { AngularFire,FirebaseDatabase } from 'angularfire2';

@Injectable()
export class AuthOuterService {

  //private headers = new Headers({'Content-Type': 'application/json'});
  private db:FirebaseDatabase;

  constructor(private _af:AngularFire/*private _http:Http*/) {
    this.db=_af.database;
  }

  signUp(newuser:any):any{
    newuser["login"]=newuser.email+newuser.password;
    return this.db.list("/ems/users").push(newuser);
  }

  sendLogin(login:any){
    return this.db.list("/ems/users",{
      query:{
        orderByChild:'login',
        equalTo:login.email+login.pass
      }
    });
    //return this._http.post(environment.urls.login,login,{headers: this.headers});
  }

}
