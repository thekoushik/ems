import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContentOuterComponent } from './content-outer/content-outer.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { AuthGuardService } from './services/auth-guard.service';

export const appRouter:Routes = [
    { path:'index', component:ContentOuterComponent },
    { path:'login', component:LoginComponent },
    { path:'signup', component:SignupComponent },
    { path:'dashboard', component: DashboardComponent,canActivate:[AuthGuardService]},
    { path:'',redirectTo:'/index',pathMatch:'full' },
    { path:'**',component:PageNotFoundComponent}
];

export const appRoutes:ModuleWithProviders = RouterModule.forRoot(appRouter);