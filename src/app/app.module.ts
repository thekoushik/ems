import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AngularFireModule } from 'angularfire2';

import { appRoutes } from './app.router';
import { AuthGuardService } from './services/auth-guard.service';

import { AppComponent } from './app.component';
import { HeaderOuterComponent } from './header-outer/header-outer.component';
import { ContentOuterComponent } from './content-outer/content-outer.component';
import { FooterOuterComponent } from './footer-outer/footer-outer.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CategoryFormComponent } from './category-form/category-form.component';

export const firebaseConfig = {
  apiKey: "AIzaSyDIL9PD8Ypa0LCM7gFDMmb-P2BYa1XQAVQ",
  authDomain: "nodetest-6df7e.firebaseapp.com",
  databaseURL: "https://nodetest-6df7e.firebaseio.com",
  storageBucket: "nodetest-6df7e.appspot.com",
  messagingSenderId: "864986626208"
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderOuterComponent,
    ContentOuterComponent,
    FooterOuterComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    PageNotFoundComponent,
    SidebarComponent,
    CategoryFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    appRoutes
  ],
  providers: [
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
