import { Component, OnInit } from '@angular/core';
//import { FormGroup } from '@angular/forms';
import { AuthOuterService } from '../services/auth-outer.service';

export class User{
  constructor(
    public fname:string,
    public lname:string,
    public email:string,
    public password:string,
    public id?:string){}
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers:[AuthOuterService]
})
export class SignupComponent implements OnInit {
  //signupform:FormGroup;

  newuser:User;

  constructor(private _authOuterService:AuthOuterService) {}

  resetUser(){
    this.newuser=new User("","","","","");
    //this.signupform.reset();
  }
  ngOnInit() {
    this.resetUser();
  }
  //database.ref("rememberit").child("notes").child("user"+uid);
  signup(){
    console.log(this.newuser);
    this._authOuterService.signUp(this.newuser).then((item)=>{
      console.log(item.key);
    });
    //this.resetUser();
  }
}
