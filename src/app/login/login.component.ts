import { Component, OnInit,Input } from '@angular/core';
import { AuthOuterService } from '../services/auth-outer.service';
import { Router } from '@angular/router';
import { AuthGuardService } from '../services/auth-guard.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers:[AuthOuterService]
})
export class LoginComponent implements OnInit {

  @Input() email:string;
  @Input() pass:string;
  
  constructor(private _authOuterService:AuthOuterService,private _router:Router,private _guard:AuthGuardService) { }

  ngOnInit() {}

  check(){
    this._authOuterService.sendLogin({email:this.email,pass:this.pass}).subscribe(snaps=>{
      if(snaps.length>0){
        this._guard.changeLogged(true);
        this._router.navigate(['/dashboard']);
      }else{
        console.log("Wrong");
      }
    });
    /*this._authOuterService.sendLogin({email:this.email,pass:this.pass}).subscribe(res=>{
      var data=res.json();
      if(data.status==1){
        this._guard.changeLogged(true);
        this._router.navigate(['/dashboard']);
      }else{
        //
      }
    },err=>{
      console.log("Error",err);
    });*/
  }
}
